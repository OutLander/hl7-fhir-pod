# HL7 FHIR Pod
![alt text](http://www.hl7.org/assets/systemimages/HL7logo.gif)
![alt text](http://www.hl7.org/implement/standards/fhir/assets/images/fhir-logo-www.png)

Fast Healthcare Interoperability Resources (FHIR, pronounced "Fire") defines a set of "Resources" that represent granular clinical concepts. The resources can be managed in isolation, or aggregated into complex documents. This flexibility offers coherent solutions for a range of interoperability problems. The simple direct definitions of the resources are based on thorough requirements gathering, formal analysis and extensive cross-mapping to other relevant standards. A workflow management layer provides support for designing, procuring, and integrating solutions. Technically, FHIR is designed for the web; the resources are based on simple XML, with an http-based RESTful protocol where each resource has predictable URL. Where possible, open internet standards are used for data representation.

The model classes in this pod are generated files from the HL7 GForge.

### References

* [FHIR Standards](http://www.hl7.org/implement/standards/fhir/index.html)
* [HL7 GFORGE](http://gforge.hl7.org/gf/)

## Recent Updates
### Version: v0.80-2286-0413
* Validation added
* Json serialization/parser not supported
* Atom Feed Added

## Installation
It is recommended to install HL7 FHIR through the [CocoaPods](http://cocoapods.org/) package manager.

Adding the private repo:

``` bash
pod repo add FHIR <url-to-the-repo-here>
```

## License

Copyright (c) 2011-2013, HL7, Inc.
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:
  
   * Redistributions of source code must retain the above copyright notice, this 
     list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, 
     this list of conditions and the following disclaimer in the documentation 
     and/or other materials provided with the distribution.
   * Neither the name of HL7 nor the names of its contributors may be used to 
     endorse or promote products derived from this software without specific 
     prior written permission.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
  POSSIBILITY OF SUCH DAMAGE.

## Credits
![alt text](http://www.mohawkcollege.ca/Sites/3/templates/images/ideaworks/ideaworks-logo.png)

HL7 FHIR Pod is brought to you by [Ideaworks](http://www.mohawkcollege.ca/ideaworks.html) at Mohawk College.
#### Contributors
* Duane Bender
* Andrew Willison
* Adam Sippel