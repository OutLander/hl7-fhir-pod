Pod::Spec.new do |s|

  s.name         = "HL7-FHIRPod"
  s.version      = "0.80-2286-0411"
  s.summary      = "Objective C implementation of HL7 FHIR."

  s.homepage     = "http://www.hl7.org/implement/standards/fhir/"
  s.license      = { :type => "Copyright (c) 2011-2013, HL7, Inc.", :file => "LICENCE" }

  s.authors      = { 
    "Andrew Willison" => "andrew.willison@mohawkcollege.ca", 
    "Adam Sippel" => "adam.sippel@mohawkcollege.ca" 
  }

  s.source       = { 
    :git => "https://andrew1234567890@bitbucket.org/andrew1234567890/hl7-fhir-pod.git", 
    :tag => "0.80-2286-0411" 
  }

  s.source_files  = "Classes/**/FHIR*.{h,m}", "Classes/**/**/FHIR*.{h,m}"
  s.exclude_files = "Example"
  s.requires_arc  = true

  s.description  = <<-DESC
    Fast Healthcare Interoperability Resources (FHIR, pronounced "Fire") defines a set of "Resources" that represent granular clinical concepts. The resources can be managed in isolation, or aggregated into complex documents. This flexibility offers coherent solutions for a range of interoperability problems. The simple direct definitions of the resources are based on thorough requirements gathering, formal analysis and extensive cross-mapping to other relevant standards. A workflow management layer provides support for designing, procuring, and integrating solutions. Technically, FHIR is designed for the web; the resources are based on simple XML, with an http-based RESTful protocol where each resource has predictable URL. Where possible, open internet standards are used for data representation.
    
    FHIR® is a registered HL7 Trademark.
                   DESC

end
