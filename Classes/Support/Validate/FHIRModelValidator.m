/*
 Copyright (c) 2011-2013, HL7, Inc.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of HL7 nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

//
//  FHIRModelValidator.m
//  FHIR Test
//
//  Created by Andrew on 2/3/2014.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import "FHIRModelValidator.h"
#import "FHIRSerializerOrderPair.h"
#import "FHIRUtilities.h"

#import "FHIRValidationResult.h"
#import "FHIRCodePatternAttribute.h"
#import "FHIRDatePatternAttribute.h"
#import "FHIRDateTimePatternAttribute.h"
#import "FHIRIdPatternAttribute.h"
#import "FHIRInstantPatternAttribute.h"
#import "FHIRNarrativeXhtmlPatternAttribute.h"
#import "FHIROidPatternAttribute.h"
#import "FHIRUriPatternAttribute.h"
#import "FHIRUuidPatternAttribute.h"

#import "FHIRResource.h"
#import "FHIRElement.h"
#import "FHIRCode.h"
#import "FHIRDate.h"
#import "FHIRDateTime.h"
#import "FHIRId.h"
#import "FHIRInstant.h"
#import "FHIRNarrative.h"
#import "FHIRXhtml.h"
#import "FHIROid.h"
#import "FHIRUri.h"
#import "FHIRUuid.h"

@interface FHIRModelValidator ()

@property (nonatomic, strong) NSMutableArray *errors;

- (void)validate:(id)object;

- (FHIRValidationResult *)validatePatternAttribute:(id)attribute withFhirType:(Class)type;
- (void)handleValidationOnItem:(id)item withItemType:(Class)itemType;
- (void)handleResult:(FHIRValidationResult *)result;

- (Class)fhirElementOrResource:(id)object;

@end

@implementation FHIRModelValidator

//+ (BOOL)validateResource:(FHIRResource *)resource {
//    
//    return NO;
//}

//+ (BOOL)validateResource:(FHIRResource *)resource withResultArray:(NSMutableArray *)resultArray {
//    
//    return NO;
//}

//+ (BOOL)validateElement:(FHIRElement *)resource {
//    
//    return NO;
//}

//+ (BOOL)validateElement:(FHIRElement *)resource withResultArray:(NSMutableArray *)resultArray {
//    
//    return NO;
//}

- (id)init
{
    self = [super init];
    if (self) {
        [self setErrors:[NSMutableArray new]];
    }
    return self;
}

+ (NSArray *) validateForErrors:(id)object {
    
    FHIRModelValidator *validator = [[FHIRModelValidator alloc] init];
    [validator validate:object];
    
    return [NSArray arrayWithArray:validator.errors];
}

- (void) validate:(id)object {
    
    NSArray *keySets = [FHIRSerializerOrderPair orderPairArrayForType:NSStringFromClass([object class])];
    
    NSDictionary *classTypes = [FHIRUtilities classTypesForObject:object];
    
    for (NSArray* keySet in keySets){
        
        NSString *key = [keySet objectAtIndex:0];
        NSString *elementKey = [NSString stringWithFormat:@"%@Element", key];
        NSString *itemTypeString = [keySet objectAtIndex:1];
        Class itemType = NSClassFromString(itemTypeString);
        Class attributeType;
        
        id value;
        
        // check if key has element access
        if ( [object respondsToSelector:NSSelectorFromString(elementKey)] ) {
            
            value = [object valueForKey:elementKey];
            attributeType = NSClassFromString([classTypes objectForKey:elementKey]);
        } else {
            
            value = [object valueForKey:key];
            attributeType = NSClassFromString([classTypes objectForKey:key]);
        }
        
        // check for value
        if ( value != nil && [itemTypeString hasPrefix:@"FHIR"] ) {
            
            NSLog(@"Validate: %@->(a:%@, i:%@, v:%@)", [object class], attributeType, itemType, NSStringFromClass([value class]));
            
            // check that the value class matches the object class
            if ( [value isKindOfClass:attributeType] ) {
                
                // validate each item in the array
                if ( attributeType == [NSArray class] ) {
                    
                    for (id item in value) {
                        
                        [self handleValidationOnItem:item withItemType:itemType];
                    }
                    
                }
                // validate single item
                else {
                    [self handleValidationOnItem:value withItemType:itemType];
                }
                
            } else {
                // Error types do not match
                [self handleResult:[[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Attribute type does not match the set object for key, %@ != %@",attributeType, [value class]]]];
            }
        }

        
        
        
        // Not Handled
        
        // Allowed Types
        // Cardinality
        // Invoke Validate Object Attribute
        
    }
}

- (void) handleValidationOnItem:(id)item withItemType:(Class)itemType {
    
    FHIRValidationResult *result = [self validatePatternAttribute:item withFhirType:itemType];
    
    if (result != nil) {
        [self handleResult:result];
    } else {
        
        // Was not validated, so validate the attributes on item
        
        NSArray *errors = [FHIRModelValidator validateForErrors:item];
        [self.errors addObjectsFromArray:errors];
    }
    
}

- (FHIRValidationResult *)validatePatternAttribute:(id)attribute withFhirType:(Class)type {
    
    if (type == [FHIRCode class]){
        
        return [[[FHIRCodePatternAttribute alloc] init] isValidValue:
                [(FHIRCode *)attribute value]
                ];
        
    } else if (type == [FHIRDate class]){
        
        return [[[FHIRDatePatternAttribute alloc] init] isValidValue:
                [(FHIRDate *)attribute value]
                ];
        
    }else if (type == [FHIRDateTime class]){
        
        return [[[FHIRDateTimePatternAttribute alloc] init] isValidValue:
                [(FHIRDateTime *)attribute value]
                ];
        
    }else if (type == [FHIRId class]){
        
        return [[[FHIRIdPatternAttribute alloc] init] isValidValue:
                [(FHIRId *)attribute value]
                ];
        
    }else if (type == [FHIRInstant class]){
        
        return [[[FHIRInstantPatternAttribute alloc] init] isValidValue:
                [(FHIRInstant *)attribute value]
                ];
        
    }else if (type == [FHIRNarrative class]){
        
        return [[[FHIRNarrativeXhtmlPatternAttribute alloc] init] isValidValue:
                [(FHIRXhtml *)[(FHIRNarrative *)attribute divElement] value]
                ];
        
    }else if (type == [FHIROid class]){
        
        return [[[FHIROidPatternAttribute alloc] init] isValidValue:
                [(FHIROid *)attribute value]
                ];
        
    }else if (type == [FHIRUri class]){
        
        return [[[FHIRUriPatternAttribute alloc] init] isValidValue:
                [(FHIRUri *)attribute value]
                ];
        
    }else if (type == [FHIRUuid class]){
        
        return [[[FHIRUuidPatternAttribute alloc] init] isValidValue:
                [(FHIRUuid *)attribute value]
                ];
        
    } else {
        
        // object is not validated.
        
        return nil;
    }
    
    
}

- (void)handleResult:(FHIRValidationResult *)result {
    
    if ([result value] != kValidationResultSuccess) {
        
        [self.errors addObject:result];
    }
    
}

- (Class)fhirElementOrResource:(id)object {
    
    Class type = [object class];
    Class result = nil;
    
    do {
        
        if ([type superclass] == [FHIRResource class]){
            
            result = [FHIRResource class];
            
        } else if ([type superclass] == [FHIRElement class]){
            
            result = [FHIRElement class];
            
        } else {
            
            type = [type superclass];
        }

    } while (type != [NSObject class]);
    
    return result;
}

@end
