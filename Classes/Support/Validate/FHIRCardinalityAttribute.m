/*
 Copyright (c) 2011-2013, HL7, Inc.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of HL7 nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

//
//  FHIRCardinalityAttribute.m
//  FHIR Test
//
//  Created by Andrew on 1/29/2014.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import "FHIRCardinalityAttribute.h"
#import "FHIRValidationResult.h"

@implementation FHIRCardinalityAttribute

- (id)init
{
    self = [super init];
    if (self) {
        [self setMinValue:0];
        [self setMaxValue:1];
    }
    return self;
}

- (FHIRValidationResult *) isValidValue:(id)value {
    
    if (value == nil) {
        
        if (self.minValue == 0) {
            
            return [[FHIRValidationResult alloc] initWithSuccess];
            
        } else {
            
            return [[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Element with min. cardinality %d cannot be null", self.minValue]];
        }
    }
    
    int count = 1;
    
    if ([value isKindOfClass:[NSArray class]]) {
        count = [value count];
    }
    
    FHIRValidationResult *result = [super isValidValue:value];
    
    if ([result value] == kValidationResultSuccess) {
        if (count < self.minValue){
            
            return [[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Element has %d elements, but min. cardinality is %d", count, self.minValue ]];
        }
    
        if (self.maxValue != -1 && count > self.maxValue){
            
            return [[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Element has %d elements, but max. cardinality is %d", count, self.maxValue]];
        }
    
    }
    return result;

    
    
}
@end
