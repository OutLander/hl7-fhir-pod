/*
 Copyright (c) 2011-2013, HL7, Inc.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of HL7 nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

//
//  FHIRUriPatternAttribute.m
//  FHIR Test
//
//  Created by Andrew on 2/3/2014.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import "FHIRUriPatternAttribute.h"
#import "FHIRValidationResult.h"
#import "FHIROidPatternAttribute.h"
#import "FHIRUuidPatternAttribute.h"

@interface FHIRUriPatternAttribute ()

- (BOOL)isAbsolute:(NSString *)url;

@end

@implementation FHIRUriPatternAttribute

- (FHIRValidationResult *)isValidValue:(id)value {
    
    if (value == nil) {
        return [[FHIRValidationResult alloc] initWithSuccess];
    }
    
    if (![value isKindOfClass:[NSString class]]) {
        @throw [NSException exceptionWithName:@"ArgumentException" reason:@"OidPatternAttribute can only be applied to string properties." userInfo:nil];
    }
    
    if ([self isAbsolute:value]) {
        // absolute
        
        if ([value hasPrefix:@"urn:oid:"] && ! [FHIROidPatternAttribute isValidString:value]) {
            
            return [[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Uri uses an urn:oid scheme, but the oid %@ is incorrect.", value]];
            
        } else if ([value hasPrefix:@"urn:uuid:"] && ! [FHIRUuidPatternAttribute isValidString:value]) {
            
            return [[FHIRValidationResult alloc] initWithFailureMessage:[NSString stringWithFormat:@"Uri uses an urn:uuid schema, but the uuid %@ is incorrect.", value]];
        }
        
        return [[FHIRValidationResult alloc] initWithSuccess];
    }
    
    return [[FHIRValidationResult alloc] initWithFailureMessage:@"Uri is not absolute."];
}

- (BOOL)isAbsolute:(NSString *)url {
    
    if (url == nil)
        return false;
    if ([url hasPrefix:@"http:"])
        return true;
    if ([url hasPrefix:@"https:"])
        return true;
    if ([url hasPrefix:@"urn:"])
        return true;
    if ([url hasPrefix:@"cid:"])
        return true;
    return false;
}

@end
